module gitlab.com/sektibayu/checkout-backend

go 1.15

require (
	github.com/go-sql-driver/mysql v1.6.0
	github.com/graphql-go/graphql v0.7.9
	github.com/joho/godotenv v1.3.0
	github.com/stretchr/testify v1.7.0
)
