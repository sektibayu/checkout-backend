package schema

import (
	"fmt"
	"math"

	"github.com/graphql-go/graphql"

	"gitlab.com/sektibayu/checkout-backend/entity"
	conn "gitlab.com/sektibayu/checkout-backend/connection"
)

var itemObject = graphql.NewObject(
	graphql.ObjectConfig{
		Name: "Item",
		Fields: graphql.Fields{
			"sku": &graphql.Field{
				Type: graphql.String,
			},
			"name": &graphql.Field{
				Type: graphql.String,
			},
			"price": &graphql.Field{
				Type: graphql.Float,
			},
			"quantity": &graphql.Field{
				Type: graphql.Int,
			},
		},
	},
)

var cartObject = graphql.NewObject(
	graphql.ObjectConfig{
		Name: "Cart",
		Fields: graphql.Fields{
			"id": &graphql.Field{
				Type: graphql.Int,
			},
			"items": &graphql.Field{
				Type: graphql.NewList(itemObject),
			},
			"total": &graphql.Field{
				Type: graphql.Float,
			},
		},
	},
)

var queryObject = graphql.NewObject(
	graphql.ObjectConfig{
		Name: "Query",
		Fields: graphql.Fields{
			"cart": &graphql.Field{
				Type:        cartObject,
				Description: "Get cart by id",
				Args: graphql.FieldConfigArgument{
					"id": &graphql.ArgumentConfig{
						Type: graphql.Int,
					},
				},
				Resolve: func(params graphql.ResolveParams) (interface{}, error) {
					id := params.Args["id"].(int)
					db := conn.InitMySQL()

					// initialize cart
					var cart entity.Cart
					cart.ID = int64(id)

					// get detail items
					selectCartResult, err := db.Query("SELECT item_id, quantity FROM cart_items WHERE id = ?;", id)
					if err != nil {
						fmt.Println("[error] error db query: ", err)
					}

					selectItemForm, err := db.Prepare("SELECT sku, name, price FROM items WHERE id = ? LIMIT 1;")
					if err != nil {
						fmt.Println("[error] error db prepare: ", err)
					}
					
					for selectCartResult.Next() {
						var item entity.Item
						selectCartResult.Scan(&item.ID, &item.Quantity)
						results, err := selectItemForm.Query(item.ID)
						if err != nil {
							fmt.Println("[error] error db query: ", err)
						}

						for results.Next() {
							results.Scan(&item.Sku, &item.Name, &item.Price)
						}
						cart.Items = append(cart.Items, item)
					}

					// calculate total price
					cart.Total = calculateTotalPrice(cart.Items)
					return cart, nil
				},
			},
		},
	})

var itemInput = graphql.NewInputObject(graphql.InputObjectConfig{
	Name: "Item Input",
	Fields: graphql.InputObjectConfigFieldMap{
		"sku": &graphql.InputObjectFieldConfig{
			Type: graphql.String,
		},
		"quantity": &graphql.InputObjectFieldConfig{
			Type: graphql.Int,
		},
	},
})

var mutationObject = graphql.NewObject(
	graphql.ObjectConfig{
		Name: "Mutation",
		Fields: graphql.Fields{
			"createCart": &graphql.Field{
				Type: cartObject,
				Description: "Create new cart",
				Args: graphql.FieldConfigArgument{
					"items": &graphql.ArgumentConfig{
						Type: graphql.NewList(itemInput),
					},
				},
				Resolve: func(params graphql.ResolveParams) (interface{}, error) {
					items := params.Args["items"].([]interface {})
					db := conn.InitMySQL()

					// initialize new cart
					var cart entity.Cart

					// generate new cart id by get last cart id + 1
					selectCartResult, err := db.Query("SELECT id FROM cart_items ORDER BY id DESC LIMIT 1;")
					if err != nil {
						fmt.Println("[error] error db query: ", err)
					}

					var last_cart_id int64
					for selectCartResult.Next() {
						selectCartResult.Scan(&last_cart_id)
					}
					cart.ID = last_cart_id + 1

					// get item detail infomation and insert to cart items
					selectItemForm, err := db.Prepare("SELECT id, sku, name, price FROM items WHERE items.sku = ? LIMIT 1;")
					if err != nil {
						fmt.Println("[error] error db prepare: ", err)
					}

					insForm, err := db.Prepare("INSERT INTO cart_items(id, item_id, quantity) VALUES(?,?,?);")
					if err != nil {
						fmt.Println("[error] error db prepare: ", err)
					}

					for _, item := range items {
						var itemResult entity.Item
						i := item.(map[string]interface{})

						results, err := selectItemForm.Query(i["sku"])
						if err != nil {
							fmt.Println("[error] error db query: ", err)
						}

						for results.Next() {
							results.Scan(&itemResult.ID, &itemResult.Sku, &itemResult.Name, &itemResult.Price)
						}
						itemResult.Quantity = int64(i["quantity"].(int))
						cart.Items = append(cart.Items, itemResult)

						insForm.Exec(cart.ID, itemResult.ID, itemResult.Quantity)
					}
					// calcuate total price and discount
					cart.Total = calculateTotalPrice(cart.Items)
					return cart, nil
				},
			},
		},
	},
)

/* 
key 1: get another item discount
key 2: get 1 item reduction discount ex: only pay 2 item cost for 3 item
key 3: percentage discount after quantity greater than specific value
key 4: a bonus item for key 1 
*/
var discountHash = map[string]entity.Discount{
	"43N23P" : {Key: 1, Quantity: 1, Bonus: "234234"},
	"120P90" : {Key: 2, Quantity: 3},
	"A304SD" : {Key: 3, Quantity: 3, DiscountPercentage: 10 },
	"234234" : {Key: 4},
}

// this variable to store bonus item from key 1
var bonusItems = map[string]int64{}

// function to calculate discount
func calculateDiscount(item entity.Item)float64{
	totalDiscount := 0.0

	// get discount type
	discount := discountHash[item.Sku]
	if discountHash == nil {
		return totalDiscount
	}

	// process with specific discount type
	if discount.Key == 1 {
		bonusItems[discount.Bonus] = item.Quantity / discount.Quantity
	}else if discount.Key == 2 {
		totalDiscount = float64((item.Quantity / discount.Quantity)) * item.Price
	}else if discount.Key == 3 {
		if item.Quantity >= discount.Quantity {
			totalDiscount = (float64(item.Quantity)*item.Price) * float64(discount.DiscountPercentage) / 100.0
		}
	}else if discount.Key == 4 {
		if bonusItems[item.Sku] != 0 {
			if bonusItems[item.Sku] > item.Quantity {
				totalDiscount = float64(item.Quantity) * item.Price
			} else {
				totalDiscount = float64(bonusItems[item.Sku]) * item.Price
			}
		}
	}
	return totalDiscount
}

// function to calculate total price
func calculateTotalPrice(items []entity.Item)float64{
	total := 0.0
	for _,item := range items {
		total += ((float64(item.Quantity) * item.Price) - calculateDiscount(item))
	}

	return math.Ceil(total*100)/100 // 2 number after point ex: 37.99
}

var CartSchema, _ = graphql.NewSchema(
	graphql.SchemaConfig{
		Query:    queryObject,
		Mutation: mutationObject,
	},
)
