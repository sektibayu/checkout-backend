package main

import (
	"fmt"
	"net/http"
	
	"gitlab.com/sektibayu/checkout-backend/handler"
	conn "gitlab.com/sektibayu/checkout-backend/connection"
)

func main() {
	http.HandleFunc("/cart", handler.CartHandler)

	fmt.Println("Server is running on port 8080")
	http.ListenAndServe(":8080", nil)
	defer conn.CloseDB()
}
