# Dependency

golang version: 1.15.5

# How to run

```
docker-compose up mysql
go run main.go
```
note there is still have a bug with just docker-compose.

# note
discount key hash:
key 1: get another item discount
key 2: get 1 item reduction discount ex: only pay 2 item cost for 3 item
key 3: percentage discount after quantity greater than specific value
key 4: a bonus item for key 1 

# Graphql Schema
```
schema {
  query: Query
  mutation: Mutation
}

type Item {
    sku: String
    name: String
    price: Float
    quantity: Int
}

type Cart {
  id: Int
  items: [Item]
  total: Float
}
type Query {
  cart (id: Int): Cart
}

type Mutation {
  createCart (items: [Item]): Cart!
  addItemToCart (id: Int, items: [Item]): Cart! # still doesn't implemented yet
}
```

# Database schema

in email

# can be improved
- move all query on repository pakage
- add case on testing
- make discount object on database.
- implement addItemToCart
- separate db seed from migration.sql
- move discount into service pakage
- move/separate resolve function on schema
- seperate db test with db production
- add ci to run main_test
- log all error

