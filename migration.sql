CREATE DATABASE IF NOT EXISTS checkout_backend;

USE checkout_backend;

CREATE TABLE IF NOT EXISTS items (
    id INT NOT NULL AUTO_INCREMENT,
    sku VARCHAR(255) UNIQUE,
    name VARCHAR(255),
    price FLOAT,
    quantity INT,
    PRIMARY KEY (id)
);

CREATE TABLE IF NOT EXISTS cart_items (
    id INT NOT NULL AUTO_INCREMENT,
    item_id INT NOT NULL,
    quantity INT,
    PRIMARY KEY(id, item_id)
);

INSERT INTO `items`(sku, name, price,quantity) VALUES('120P90','Google Home', 49.99, 10);
INSERT INTO `items`(sku, name, price,quantity) VALUES('43N23P','Macbook Pro', 5399.99, 5);
INSERT INTO `items`(sku, name, price,quantity) VALUES('A304SD','Alexa Speaker', 109.50, 10);
INSERT INTO `items`(sku, name, price,quantity) VALUES('234234','Raspberry Pi B', 30.00, 2);
