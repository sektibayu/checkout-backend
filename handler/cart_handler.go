package handler

import (
	"fmt"
	"net/http"
	"encoding/json"

	"github.com/graphql-go/graphql"

	"gitlab.com/sektibayu/checkout-backend/schema"
)

// struct to help decode graphQL request body
type reqBody struct {
	Query string `json:"query"`
}

// function to execute graphql query
func executeQuery(query string, schema graphql.Schema) *graphql.Result {
	result := graphql.Do(graphql.Params{
		Schema:        schema,
		RequestString: query,
	})
	if len(result.Errors) > 0 {
		fmt.Printf("errors: %v", result.Errors)
	}
	return result
}

func CartHandler(w http.ResponseWriter, r *http.Request){
	var rBody reqBody
	err := json.NewDecoder(r.Body).Decode(&rBody)
	if err != nil {
		fmt.Println("[error] error parse body: ", err)
	}
	result := executeQuery(rBody.Query, schema.CartSchema)
	json.NewEncoder(w).Encode(result)
}
