package entity

type Discount struct {
	Key int64
	Quantity int64
	Bonus string
	DiscountPercentage int64
}
