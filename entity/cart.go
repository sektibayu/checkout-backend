package entity

type Cart struct {
	ID   				int64 	`json:"id"`
	Items	 			[]Item 	`json:items`
	Total			 	float64 `json:total`
}
