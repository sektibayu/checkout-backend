package entity

type Item struct {
	ID				int64 	`json:"id"`
	Sku 			string 	`json:"sku"`
	Name 			string 	`json:"name"`
	Price 		float64 `json:"price"`
	Quantity 	int64 	`json:"quantity"`
}
