package connection

import(
	"os"
	"fmt"
	"database/sql"

	"github.com/joho/godotenv"
  _ "github.com/go-sql-driver/mysql"
)

var database *sql.DB

func InitMySQL() *sql.DB {
	if database == nil {
		godotenv.Load()
		database_user := os.Getenv("DATABASE_USER")
		database_password := os.Getenv("DATABASE_PASSWORD")
		database_host := os.Getenv("DATABASE_HOST")
		database_port := os.Getenv("DATABASE_PORT")
		database_name := os.Getenv("DATABASE_NAME")

		db, err := sql.Open("mysql", fmt.Sprintf("%s:%s@tcp(%s:%s)/%s", database_user, database_password, database_host, database_port, database_name))
		database = db //assign to database variable
		if err != nil {
					fmt.Println("[error] Error database connection")
					panic(err.Error())
    }
	}
	return database
}

func CloseDB() error {
	return database.Close()
}
