FROM golang:1.15-alpine
WORKDIR /go/src/checkout-backend
COPY . .
COPY ./env.sample ./.env
RUN go build -o checkout-backend
ENTRYPOINT [ "./checkout-backend" ]
EXPOSE 8080
