package main

import (
	"net/http"
	"net/http/httptest"
	"testing"
	"bytes"

	"github.com/stretchr/testify/assert"

	"gitlab.com/sektibayu/checkout-backend/handler"
	conn "gitlab.com/sektibayu/checkout-backend/connection"
)

func TestHandler(t *testing.T) {
	router := http.HandlerFunc(handler.CartHandler)
	db := conn.InitMySQL()
	db.Exec("TRUNCATE cart_items;")
	payload := bytes.NewBuffer([]byte(`{"query":"mutation {createCart(items: [{sku: \"120P90\",quantity: 3}]) { id items { sku name price quantity } total }}"}`))
	w := httptest.NewRecorder()
	req, _ := http.NewRequest("POST", "/cart", payload)
	req.Header.Set("Content-Type", "application/json")
	router.ServeHTTP(w, req)

	assert.Equal(t, 200, w.Code)
	assert.JSONEq(t, `{"data":{"createCart":{"id":1,"items":[{"name":"Google Home","price":49.99,"quantity":3,"sku":"120P90"}],"total":99.98}}}`, w.Body.String())
}
